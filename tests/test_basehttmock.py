# SPDX-FileCopyrightText: 2021 Iwan Aucamp
#
# SPDX-License-Identifier: MIT-0 OR CC0-1.0

import unittest
from urllib.error import HTTPError
from urllib.request import Request, urlopen

from httpservermock import (
    BaseHTTPServerMock,
    MethodName,
    MockHTTPResponse,
    ctx_http_server,
)


class BaseHTTPMockTests(unittest.TestCase):
    def test_example(self) -> None:
        httpmock = BaseHTTPServerMock()
        with ctx_http_server(httpmock.Handler) as server:
            url = "http://{}:{}".format(*server.server_address)
            # add two responses the server should give:
            httpmock.responses[MethodName.GET].append(
                MockHTTPResponse(404, "Not Found", b"gone away", {})
            )
            httpmock.responses[MethodName.GET].append(
                MockHTTPResponse(200, "OK", b"here it is", {})
            )

            # send a request to get the first response
            with self.assertRaises(HTTPError) as raised:
                urlopen(f"{url}/bad/path")
            assert raised.exception.code == 404

            # get and validate request that the mock received
            req = httpmock.requests[MethodName.GET].pop(0)
            self.assertEqual(req.path, "/bad/path")

            # send a request to get the second response
            resp = urlopen(f"{url}/")
            self.assertEqual(resp.status, 200)
            self.assertEqual(resp.read(), b"here it is")

            req = httpmock.requests[MethodName.GET].pop(0)
            self.assertEqual(req.path, "/")

            self.assertEqual(httpmock.call_count, 2)

            httpmock.reset()

            httpmock.responses[MethodName.PATCH].append(
                MockHTTPResponse(
                    200, "OK", b"patched", {"Authorization": ["Bearer faketoken"]}
                )
            )

            send_req = Request(
                f"{url}/", method=MethodName.PATCH.name, data=b"new value"
            )
            resp = urlopen(send_req)
            self.assertEqual(resp.status, 200)
            self.assertEqual(resp.read(), b"patched")
            self.assertEqual(resp.headers.get("Authorization"), "Bearer faketoken")

            req = httpmock.requests[MethodName.PATCH].pop(0)
            self.assertEqual(req.path, "/")
            self.assertEqual(req.body, send_req.data)

            self.assertEqual(httpmock.call_count, 1)
